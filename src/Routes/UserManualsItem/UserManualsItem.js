import React, {useEffect} from 'react'

import {Link} from 'react-router-dom';
import ImageGallery from 'react-image-gallery';
import Header from 'components/Header';
import ScrollingBody from 'components/ScrollingBody';
import _ from 'lodash';
import "react-image-gallery/styles/css/image-gallery.css";
import './styles.module.scss';
import {API_URL} from "utils";
import {ROUTES as Routes} from "routes";

const UserManualsItem = ({fetchStaffItem, staff}) => {

    useEffect(() => {
        fetchStaffItem(window.location.pathname, ['rectangle_medium', 'staff_prev']);
    }, []);

        let url = window.location.pathname + window.location.search;
        let item = _.get(staff, url + '.item', []);
        let galleryFiles = _.filter(item.files, {type: 'image'}),
            mainImage = _.find(item.files, {type: 'image', is_main: '1'}),
            instruction = _.find(item.files, {type: 'document'});

        return (
            <div className="instructionsCompItem">
                <Header location={window.location}/>
                <ScrollingBody>
                    <div className="instructionsCompItem__staffItem">
                        <div className="instructionsCompItem__staffItem__wrapper">
                            <div className="instructionsCompItem__backButton">
                                <Link to={Routes.userManuals}><span>&larr;</span>Назад к списку инструкций</Link>
                            </div>
                            <div className="instructionsCompItem__gallery">
                                {
                                    galleryFiles.length === 1
                                        ? (<div className="mainImage"><img
                                            src={API_URL + mainImage.rectangle_medium} alt={item.title}/></div>)
                                        : null
                                }
                                {
                                    galleryFiles.length > 1
                                        ? (

                                            <ImageGallery
                                                items={_.map(_.filter(item.files, {type: 'image'}), file => {
                                                    return {
                                                        original: API_URL + file.rectangle_medium,
                                                        thumbnail: API_URL + file.staff_prev
                                                    };
                                                })}
                                            />

                                        )
                                        : null
                                }
                            </div>
                            <div className="instructionsCompItem__info">
                                <div className="instructionsCompItem__title">
                                    {item.title}
                                </div>
                                <div className="instructionsCompItem__description">
                                    {item.description}
                                </div>
                                <div className="instructionsCompItem__content"
                                     dangerouslySetInnerHTML={{__html: item.content}}/>
                                {instruction
                                    ? (<a className="instruction_field" download=""
                                          href={API_URL + '/' + instruction.path + '/download/' + instruction.title}>Скачать
                                        инструкцию</a>)
                                    : null}
                                {instruction
                                    ? (<a className="instruction_field" target="_blank"
                                          href={API_URL + '/' + instruction.path + '/' + instruction.title}>Просмотреть
                                        инструкцию</a>)
                                    : null}
                            </div>
                        </div>
                    </div>
                </ScrollingBody>
            </div>
        );
}

export default UserManualsItem
