import { connect } from 'react-redux'
import { actions } from 'redux/staff';

import Instructions from './Instructions'

const mapStateToProps = (state) => ({
    staff: state.staff.staff,
    fetching: state.staff.fetching
});

export default connect(mapStateToProps, Object.assign({}, actions))(Instructions)
