import React from 'react'

import {Link} from 'react-router-dom';

import Header from 'components/Header';
import ScrollingBody from 'components/ScrollingBody';
import Preloader from 'components/Preloader';

import _ from 'lodash';

import './instructions.module.scss'
import {API_URL} from "../../utils";

class Instructions extends React.Component {
    state = {
        viewByType: ''
    };

    componentDidMount() {
        this.props.fetchStaff(window.location.pathname, {schemes: 'staff_prev_4-3,staff_prev_png,staff_a4'});
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.viewByType !== this.state.viewByType) {
            this.props.fetchStaff(window.location.pathname + this.state.viewByType, {schemes: 'staff_prev_4-3,staff_prev_png,staff_a4'});
        }
    }

    componentWillUnmount() {

    }

    changeViewType(type, e) {
        e.preventDefault();
        this.setState({viewByType: type});
    }

    render() {
        let url = window.location.pathname + this.state.viewByType;
        let staff = _.get(this.props.staff, url + '.items', []),
            catalogue = _.get(this.props.staff, url + '.catalog', {});

        return (
            <div className="usefulComp">
                <Header location={window.location}/>
                <ScrollingBody>
                    <div className="wrapper">
                        <div className="usefulComp__items">
                            <div className="usefulComp__description">
                                <div className="description">{catalogue.description}</div>
                                <div className="description">{catalogue.content}</div>
                            </div>
                            {_.map(staff, item => {
                                return (
                                    <div className={'usefulComp__item ' + item.catalog_type}>
                                        <div className="image">
                                            <div className="image__wrapper">
                                                <img
                                                    src={API_URL + _.get(_.find(item.file, {is_main: '1'}), (item.catalog_type === 'video' ? 'staff_prev_4-3' : 'staff_a4'))} alt={item.title}/>
                                            </div>
                                        </div>
                                        <div className="title">{item.title}</div>
                                        <Link className="readon" to={'/instructions/' + item.label + '-item'}/>
                                        {item.second_title !== ''
                                            ?
                                            (<div className="second-title">{item.second_title}</div>)
                                            :
                                            null
                                        }


                                    </div>);
                            })}
                        </div>
                        <Preloader trigger={_.some(this.props.fetching, (f) => {
                            return f;
                        })}/>
                    </div>
                </ScrollingBody>
            </div>
        );
    }
}

export default Instructions

