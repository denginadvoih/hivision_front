import { connect } from 'react-redux'
import { actions } from 'redux/staff';
import { actions as refActions } from 'redux/reference';

import LampSelection from './LampSelection'

const mapStateToProps = (state) => ({
    marks: state.reference.marks,
    models: state.reference.models,
    modelNumbers: state.reference.modelNumbers,
    modelGenerations: state.reference.modelGenerations,
    lamps: state.reference.lamps,
    types: state.reference.types,
    pimps: state.reference.pimps,
    staff: state.staff.staff,
    catalogueTree: state.staff.catalogueTree
});

export default connect(mapStateToProps, Object.assign({}, actions, refActions))(LampSelection)
