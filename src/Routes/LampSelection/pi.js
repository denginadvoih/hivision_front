/**
 * Created by alexej on 14.11.2017.
 */
var itIsCalled = false;

function reset_lamp_popup_content() {
    $(".lamp-card-types li:gt(0), .lamp-card-tabs li:gt(0), .lamp-card-tab-content:gt(0)", ".lamp-card-container").remove();
    $("#lamps-menu-left li, #lamps-menu-right li").removeClass("active");
    $(".lamp-cards").removeClass("lamp-cards");
    if (itIsCalled)
        $.colorbox.remove();
    $("ul.lamp-card-tabs li").unbind("click");
    $("img", "#lamps-menu-left, #lamps-menu-right").each(function(){
        var oldSrc = $(this).attr("src");
        if (oldSrc.indexOf("-gr") == -1)
            $(this).attr("src", oldSrc.replace("lamp-name", "lamp-name-gr"));
    });
    $("#catalogue-results").addClass("no-results");
    itIsCalled = false;
    return false;
}

function build_lamp_popup_content(name, data) {

    var obj = data.list;
    // if ('bysubtype' in data) {
    //     obj = data.bysubtype
    // }
    obj = data.bysubtype;
    var $myDiv = $("#" + name);

    // lamp buttons
    var $lamp_btn = $("a[href='#" + name + "']");
    $lamp_btn.addClass("lamp-cards").parent().addClass("active");
    var oldSrc = $lamp_btn.children("img").attr("src");
    $lamp_btn.children("img").attr("src", oldSrc.replace("-gr", ""));
    $("#catalogue-results").removeClass("no-results");

    var firstGroup = true;
    var firstItem = true;
    $.each(obj, function(groupkey, group) {
        if (!firstGroup) {
            $(".lamp-card-types li:first", $myDiv).clone().appendTo($(".lamp-card-types", $myDiv)).removeClass("activecardtype");
        }
        $(".lamp-card-types li:last a", $myDiv).attr("href", "#" + groupkey).text(group.name).parent().attr('groupid', groupkey);

        $.each(group.items, function(key, val) {
            // for (var i = 0; i < obj.length; i++) {
            var props = {
                article: $.trim(val.article),
                type: val.type,
                power: val.power,
                pump: val.pump,
                glass: val.glass,
                // LightColor: val.LightColor,
                // Summary: val.Summary,
                image: '/uploads/catalogkoito/' + val.image
            };

            if (!firstItem) {
                $(".lamp-card-tabs li:first", $myDiv).clone().removeClass("activecardtab").appendTo($(".lamp-card-tabs", $myDiv));
                $(".lamp-card-tab-content:first", $myDiv).clone().appendTo($(".lamp-card-tab-container", $myDiv));
            }
            // Tabs
            $(".lamp-card-tabs li:last a", $myDiv).attr("href", "#" + props.article).text(props.article);
            $(".lamp-card-tabs li:last", $myDiv).attr('groupid', groupkey).hide();


            $("ul.lamp-card-types li").unbind('click').click(function() {
                var thisgroup = $(this).attr('groupid');
                var $myDiv = $(this).parent().parent();
                $("ul.lamp-card-types li").removeClass("activecardtype"); //Remove any "active" class
                $(this).addClass("activecardtype"); //Add "active" class to selected tab
                $(".lamp-card-tab-content").hide(); //Hide all tab content
                $(".lamp-card-tabs li", $myDiv).hide();
                $(".lamp-card-tabs li[groupid*='" + thisgroup + "']", $myDiv).show();

                $(".lamp-card-tabs li[groupid*='" + thisgroup + "'] a", $myDiv).first().click();
                return false;
            });


            $("ul.lamp-card-tabs li").unbind('click').click(function() {
                // if ($(this).hasClass("activecardtab")) {
                //     return false;
                // }
                $("ul.lamp-card-tabs li").removeClass("activecardtab"); //Remove any "active" class
                $(this).addClass("activecardtab"); //Add "active" class to selected tab
                $(".lamp-card-tab-content").hide(); //Hide all tab content
                var activeTab = $(this).find("a").attr("href") + '.lamp-card-tab-content'; //Find the rel attribute value to identify the active tab + content
                $(activeTab).fadeIn(0); //Fade in the active content
                return false;
            });
            //Content
            var $currentDiv = $(".lamp-card-tab-content:last", $myDiv);
            $currentDiv.attr("id", props.article).attr('groupid', groupkey);
            $(".code", $currentDiv).text(props.article);
            $(".kind", $currentDiv).text(props.type);
            $(".power", $currentDiv).text(props.power);
            $(".lamp-card-img img", $currentDiv).attr({
                alt: $(".lamp-title", $currentDiv).text() + " " + props.article + " " + props.type + " " + props.power,
                src: props.image
            });
            $(".socket", $currentDiv).text(props.pump);
            $(".glass", $currentDiv).text(props.glass);
            // $(".desc", $currentDiv).text(props.Summary);

            firstItem = false;
        });
        if (firstGroup) {
            $(".lamp-card-tabs li[groupid*='" + groupkey + "']", $myDiv).show();
        }
        firstGroup = false;
    });
    $(".lamp-cards").colorbox({opacity:0.8, speed:500, inline:true, rel:'lamp-cards', current: "",
        // innerHeight: "280px",
        onLoad: function() {
            $("ul.lamp-card-types li:first", $($(this).prop('hash') + '.lamp-card-container')).click()
            $(this).colorbox.resize();
        },
        onComplete: function() {
            // $("ul.lamp-card-tabs li:first", $($(this).prop('hash') + '.lamp-card-container')).click()
            // $(this).colorbox.resize();
        }
    });
    itIsCalled = true;
    return false;
}

$(document).ready(function () {

    $("select").each(function() {
        $("option:first", this).attr("selected", "selected");
    });

    // JSON для моделей машин
    $("#select-vendor option:first").attr("selected", "selected");
    $("#select-vendor").change(function() {
        if ($(this).val()) {
            reset_lamp_popup_content();
            $("#select-model, #select-number, #select-year").empty();
            var $parent = $("#select-model").parent();
            var url = "/catalog/children";
            $.ajax({
                url: url,
                data: "slug=" + $(this).val(),
                beforeSend: function() {
                    $(".ajax-loader", $parent).show();
                },
                success: function(data) {
                    $("#select-model").append("<option disabled='disabled'>Название модели</option>");
                    for (var i = 0; i < data.length; i++) {
                        $("#select-model").append("<option value='" + data[i]['slug'] + "'>" + data[i]['title'] + "</option>");
                    }
                    $("#select-model option:first").attr("selected", "selected");
                },
                complete: function() {
                    $(".ajax-loader", $parent).hide();
                }
            });
        }
    });


    // Собираем селект для номера модели
    $("#select-model").change(function() {
        if ($(this).val()) {
            reset_lamp_popup_content();
            $("#select-number, #select-year").empty();
            var $parent = $("#select-number").parent();
            var url = "/catalog/children";
            $.ajax({
                url: url,
                data: "slug=" + $(this).val(),
                beforeSend: function() {
                    $(".ajax-loader", $parent).show();
                },
                success: function(data) {
                    $("#select-number").append("<option disabled='disabled'>Номер модели</option>");
                    for (var i = 0; i < data.length; i++) {
                        $("#select-number").append("<option value='" + data[i]['slug'] + "'>" + data[i]['title'] + "</option>");
                    }
                    $("#select-number option:first").attr("selected", "selected");
                },
                complete: function() {
                    $(".ajax-loader", $parent).hide();
                }
            });
        }
    });

    // Собираем селект для даты
    $("#select-number").change(function() {
        if ($(this).val()) {
            reset_lamp_popup_content();
            $("#select-year").empty();
            var $parent = $("#select-year").parent();
            var url = "/catalog/items";
            $.ajax({
                url: url,
                data: "slug=" + $(this).val(),
                beforeSend: function() {
                    $(".ajax-loader", $parent).show();
                },
                success: function(data) {
                    $("#select-year").append("<option disabled='disabled'>Дата выпуска</option>");
                    for (var i = 0; i < data.length; i++) {
                        $("#select-year").append("<option value='" + data[i]['slug'] + "'>" + data[i]['title'] + "</option>");
                    };
                    $("#select-year option:first").attr("selected", "selected");
                },
                complete: function() {
                    $(".ajax-loader", $parent).hide();
                }
            });
        }
    });

    // Magic
    $("#select-year").change(function() {
        if ($(this).val()) {
            reset_lamp_popup_content();
            var dateRange = $(this).val();
            var url = '/catalog/item';
            $parent = $("#select-year").parent();
            $.ajax({
                url: url,
                data: "slug=" + dateRange,
                beforeSend: function() {
                    $(".ajax-loader", $parent).show();
                },
                success: function (data) {
                    if (data != null)
                        var lamps = data.data;
                    $.each(lamps, function(name, data) {
                        build_lamp_popup_content(name, data);
                    });
                },
                complete: function() {
                    $(".ajax-loader", $parent).hide();
                }
            });
        }
    });
});

</script>
