import React from 'react'

import cn from 'classnames';

import Header from 'components/Header';
import ScrollingBody from 'components/ScrollingBody';
import Select from 'components/Select';
import LampItem from 'components/LampItem';

import _ from 'lodash';

import classes from './lampselection.module.scss';

class LampSelection extends React.Component {
    state = {
        mark: null,
        model: null,
        modelNumber: null,
        modelGeneration: null,
        showLamp: null,
        staffUrl: null
    };

    componentDidMount() {
        this.props.fetchTypes();
        this.props.fetchCarMarks();
    }

    getTypeField(title) {
        let id = _.get(this.props.types, 'id');
        let rItems = _.get(this.props.types, 'reference.items'),
            rItem = _.find(rItems, ri => {
                return ri.title.toLowerCase() === title.toLowerCase();
            });
        return {id: id, value: _.get(rItem, 'id', null)};
    }

    fetchStaff(fields) {
        let request = [];
        if (fields.type) {
            request.push(this.getTypeField(fields.type));
        }
        let requestString = _.map(request, param => {
            return 'fields[' + param.id + ']=' + param.value;
        }).join('&');
        this.setState({staffUrl: '/catalogue?' + requestString});
        this.props.fetchStaff('/catalogue?' + requestString);
    }

    render() {
        let marks = this.props.marks,
            models = _.get(this.props.models, this.state.mark, []),
            modelNumbers = _.get(this.props.modelNumbers, this.state.model, []),
            modelGenerations = _.get(this.props.modelGenerations, this.state.modelNumber, []);
        let carLamps = [
            {title: 'Задние габариты', label: 'rearmarker', position: 'left'},
            {title: 'Лампа стопсигнала', label: 'stop', position: 'left'},
            {title: 'Лампа заднего хода', label: 'rear', position: 'left'},
            {title: 'Заднее освещение', label: 'innerrear', position: 'left'},
            {title: 'Повторитель стоп сигнала', label: 'stoprepeater', position: 'left'},
            {title: 'Подсветка номера', label: 'numberlights', position: 'left'},
            {title: 'Задний поворотник', label: 'turnrear', position: 'left'},
            {title: 'Противотуманная фара', label: 'rearantifog', position: 'left'},
            {title: 'Лампа ближнего света', label: 'headclose', position: 'right'},
            {title: 'Лампа дальнего света', label: 'headfar', position: 'right'},
            {title: 'Лампа габаритов', label: 'marker', position: 'right'},
            {title: 'Центральный свет', label: 'innermiddle', position: 'right'},
            {title: 'Переднее освещение', label: 'innerfront', position: 'right'},
            {title: 'Боковой поворотник', label: 'turnside', position: 'right'},
            {title: 'Передний поворотник', label: 'turnfront', position: 'right'},
            {title: 'Противотуманная фара', label: 'antifog', position: 'right'},
            {title: 'Повторитель поворотников', label: 'repeaters', position: 'right'}
        ];

        let lamps = _.get(this.props.lamps, this.state.modelGeneration, {}),
            lampData = this.state.showLamp ? _.find(lamps, {purpose: this.state.showLamp}) : null,
            staff = this.state.staffUrl ? _.get(this.props.staff, this.state.staffUrl, []) : null;
        staff = staff ? _.get(staff, 'items', []) : null;

        let types = [
            {value: '702K', label: '702K', type: 'item'},
            {value: '9005 (HB3)', label: '9005 (HB3)', type: 'item'},
            {value: '9005J', label: '9005J', type: 'item'},
            {value: '9006 (HB4)', label: '9006 (HB4)', type: 'item'},
            {value: '9006J', label: '9006J', type: 'item'},
            {value: 'D2R', label: 'D2R', type: 'item'},
            {value: 'D2S', label: 'D2S', type: 'item'},
            {value: 'D4', label: 'D4', type: 'item'},
            {value: 'D4R', label: 'D4R', type: 'item'},
            {value: 'D4S', label: 'D4S', type: 'item'},
            {value: 'H1', label: 'H1', type: 'item'},
            {value: 'H11', label: 'H11', type: 'item'},
            {value: 'H11 (H8, H16)', label: 'H11 (H8, H16)', type: 'item'},
            {value: 'H16', label: 'H16', type: 'item'},
            {value: 'H16 (H8, H11)', label: 'H16 (H8, H11)', type: 'item'},
            {value: 'H2', label: 'H2', type: 'item'},
            {value: 'H27/1', label: 'H27/1', type: 'item'},
            {value: 'H27/2', label: 'H27/2', type: 'item'},
            {value: 'H3', label: 'H3', type: 'item'},
            {value: 'H3A', label: 'H3A', type: 'item'},
            {value: 'H3c', label: 'H3c', type: 'item'},
            {value: 'H3d', label: 'H3d', type: 'item'},
            {value: 'H3R', label: 'H3R', type: 'item'},
            {value: 'H4', label: 'H4', type: 'item'},
            {value: 'H4U', label: 'H4U', type: 'item'},
            {value: 'H6', label: 'H6', type: 'item'},
            {value: 'H7', label: 'H7', type: 'item'},
            {value: 'H8', label: 'H8', type: 'item'},
            {value: 'H9', label: 'H9', type: 'item'},
            {value: 'HS1', label: 'HS1', type: 'item'},
            {value: 'IH01', label: 'IH01', type: 'item'},
            {value: 'MH6', label: 'MH6', type: 'item'},
            {value: 'MH6R', label: 'MH6R', type: 'item'},
            {value: 'R2', label: 'R2', type: 'item'}
        ];

        return (
            <div className={classes.lampSelectionComp}>
                <Header location={window.location}/>
                <ScrollingBody>
                    <div className={classes.autofilter}>
                        <div className={classes.wrapper}>
                            <div className={classes.title}>Данные вашего автомобиля</div>
                            <div className={classes.filter}>
                                <Select
                                    placeholder="Марка авто"
                                    type="select"
                                    value={this.state.mark}
                                    options={_.map(marks, mark => {
                            return {label: mark.title, value: mark.id, type: 'item'};
                        })}
                                    onChange={(mark) => {
                                this.props.fetchCarModels(mark.value);
                                this.setState({mark: mark.value});
                            }}
                                />
                            </div>
                            <div className={classes.filter}>
                                <Select
                                    placeholder="Модель авто"
                                    type="select"
                                    value={this.state.model}
                                    options={_.map(models, model => {
                            return {label: model.title, value: model.id, type: 'item'};
                        })}
                                    onChange={(model) => {
                                this.props.fetchCarModelNumbers(model.value);
                                this.setState({model: model.value});
                            }}
                                />
                            </div>
                            <div className={classes.filter}>
                                <Select
                                    placeholder="Номер модели"
                                    type="select"
                                    value={this.state.modelNumber}
                                    options={_.map(modelNumbers, number => {
                            return {label: number.title, value: number.id, type: 'item'};
                        })}
                                    onChange={(modelNumber) => {
                                this.props.fetchCarModelGenerations(modelNumber.value);
                                this.setState({modelNumber: modelNumber.value});
                            }}
                                />
                            </div>
                            <div className={classes.filter}>
                                <Select
                                    placeholder="Года выпуска"
                                    type="select"
                                    value={this.state.modelGeneration}
                                    options={_.map(modelGenerations, generation => {
                            return {label: generation.title, value: generation.id, type: 'item'};
                        })}
                                    onChange={(modelGeneration) => {
                                    this.props.fetchLamps(modelGeneration.value);
                                this.setState({modelGeneration: modelGeneration.value});
                            }}
                                />
                            </div>
                        </div>

                    </div>
                    <div className={classes.body}>
                        <div className={classes.lightPoligon}>
                            {carLamps.map(lamp => {
                                    let thatLamp = _.find(lamps, {purpose: lamp.label}, false);
                                    return (
                                        <a key={lamp.label} data-label={lamp.label}
                                           className={cn({[classes.lamp]: true, [classes[lamp.position]]: true, [classes.active]: thatLamp})}
                                           onClick={(e) => {
                                        e.preventDefault();
                                        if (thatLamp) {
                                        this.setState({showLamp: lamp.label});
                                        this.fetchStaff({type: thatLamp.type});
                                        }
                                       }}
                                           href={'#' + lamp.label}>
                                            <div className={classes.text}>{lamp.title}</div>
                                        </a>
                                    );
                                }
                            )}
                            <div className={classes.searchedParams}>
                                {this.state.showLamp ? (<div>Тип: {lampData.type} Цоколь: {lampData.pimp}</div>) : null}
                            </div>
                        </div>
                    </div>
                    <div className={classes.additionFilter}>
                        <div className={classes.filter}>
                            <div className={classes.label}>Подбор по классификации</div>
                            <div className={classes.input}>
                                <div className={classes.input}>
                                    <Select
                                        placeholder="Выберите классификацию"
                                        type="select"
                                        value={this.state.searchByType}
                                        options={types}
                                        onChange={(type) => {
                                this.setState({searchByType: type.value});
                                this.fetchStaff({type: type.label});
                            }}
                                    />
                                </div>
                                <div className={classes.submit}>Найти</div>
                            </div>
                        </div>
                    </div>
                    <div className={classes.staff}>
                        {(this.state.staffUrl && staff && staff.length === 0) ? (
                            <div className={classes.empty}>Ничего не найдено</div>) : null}
                        {_.map(staff, item => {
                            return (<LampItem item={item} key={item.id}/>);
                        })}
                    </div>
                </ScrollingBody>
            </div>
        );
    }
}

export default LampSelection;
