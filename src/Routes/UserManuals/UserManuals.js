import React, {useEffect, useState} from 'react'

import {Link} from 'react-router-dom';

import Header from 'components/Header';
import ScrollingBody from 'components/ScrollingBody';
import Preloader from 'components/Preloader';

import _ from 'lodash';

import './styles.module.scss'
import {useLocation} from "react-router-dom";
import {API_URL} from 'utils';

const UserManuals = ({fetchStaff, propsStaff, fetching}) => {
    const location = useLocation()
    const [viewByType, setViewByType] = useState('');

    useEffect(() => {
        fetchStaff(location.pathname + viewByType, {schemes: 'staff_prev_4-3,staff_prev_png,staff_a4'});
    }, [location, viewByType]);

    let url = window.location.pathname + viewByType;
    let staff = _.get(propsStaff, url + '.items', []),
        catalogue = _.get(propsStaff, url + '.catalog', {});

    return (
        <div className="usefulComp">
            <Header location={window.location}/>
            <ScrollingBody>
                <div className="wrapper">
                    <div className="usefulComp__items">
                        <div className="usefulComp__description">
                            <div className="description">{catalogue.description}</div>
                            <div className="description">{catalogue.content}</div>
                        </div>
                        {_.map(staff, item => {
                            return (
                                <div className={'usefulComp__item ' + item.catalog_type}>
                                    <div className="image">
                                        <div className="image__wrapper">
                                            <img
                                                src={API_URL + _.get(_.find(item.file, {is_main: '1'}), (item.catalog_type === 'video' ? 'staff_prev_4-3' : 'staff_a4'))}/>
                                        </div>
                                    </div>
                                    <div className="title">{item.title}</div>
                                    <Link className="readon" to={'/user-manuals/' + item.label + '-item'}/>
                                    {item.second_title !== ''
                                        ?
                                        (<div className="second-title">{item.second_title}</div>)
                                        :
                                        null
                                    }
                                </div>);
                        })}
                    </div>
                    <Preloader trigger={_.some(fetching, (f) => {
                        return f;
                    })}/>
                </div>
            </ScrollingBody>
        </div>
    );
}

export default UserManuals

