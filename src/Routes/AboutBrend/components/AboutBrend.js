import React from 'react'

import {Link} from 'react-router';

import Header from 'components/Header';
import ScrollingBody from 'components/ScrollingBody';


import _ from 'lodash';
import moment from 'moment';

import '../components/aboutbrend.scss'

class AboutBrend extends React.Component {

    componentDidMount() {
        this.props.fetchStaff(window.location.pathname, {schemes: 'staff_prev_4-3,staff_prev_png,staff_a4'});
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.viewByType !== this.state.viewByType) {
            this.props.fetchStaff(window.location.pathname + this.state.viewByType, {schemes: 'staff_prev_4-3,staff_prev_png,staff_a4'});
        }
    }

    componentWillUnmount() {

    }

    render() {
        let url = window.location.pathname;
        let staff = _.get(this.props.staff, url + '.items', []),
            catalogue = _.get(this.props.staff, url + '.catalog', {});
        return (
            <div className="aboutbrendComp">
                <Header location={window.location}/>
                <ScrollingBody>
                    <div className="aboutbrendComp__items">
                        <div className="aboutbrendComp__description">
                            <div className="description">{catalogue.description}</div>
                            <div className="content" dangerouslySetInnerHTML={{__html: catalogue.content}}/>
                        </div>
                        {_.map(staff, item => {
                            return (
                                <div className="aboutbrendComp__item">
                                    <div className="date">{moment(item.date).locale('ru').format('DD MMMM')}</div>
                                    <div className="image">
                                        <div className="image__wrapper">
                                            <img
                                                src={API_URL + _.get(_.find(item.file, {is_main: '1'}), (item.catalog_type === 'video' ? 'staff_prev_4-3' : 'staff_a4'))} alt={item.title}/>
                                        </div>
                                    </div>
                                    <div className="title">{item.title}</div>
                                    <Link  className="readon" to={'/aboutbrend/' + item.label + '-item'}/>
                                    {item.second_title !== ''
                                        ?
                                        (<div className="second-title">{item.second_title}</div>)
                                        :
                                        (<div className="second-title second-title--links">{allLinks[item.catalog_type]}</div>)
                                    }


                                </div>);
                        })}
                    </div>
                </ScrollingBody>
            </div>
        );
    }
}

AboutBrend.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default AboutBrend

