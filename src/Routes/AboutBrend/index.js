export default (store) => ({
    path: 'about-brend',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {
            const Cooperation = require('./containers/AboutBrendContainer').default;
            cb(null, Cooperation)
        }, 'AboutBrend')
    }
})