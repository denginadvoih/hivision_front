import { connect } from 'react-redux'
import { actions } from 'redux/staff';

import AboutBrend from '../components/AboutBrend'

const mapStateToProps = (state) => ({
    staff: state.staff.staff
});

export default connect(mapStateToProps, Object.assign({}, actions))(AboutBrend)
