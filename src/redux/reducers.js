import { combineReducers } from "redux";
import hangar from "./hangar.js";
import location from './location';
import reference from './reference';
import staff from './staff';

export default combineReducers({ hangar, location, reference, staff });