import React from 'react'

import {Scrollbar} from 'react-scrollbars-custom';

import classes from './scrollingbody.module.scss'

class ScrollingBody extends React.Component {

    renderTrack({style, ...props}) {
        const trackStyle = {};
        return (
            <div className="scrollTrack"
                 style={{...style, ...trackStyle}}
                 {...props}></div>
        );
    }

    renderThumb({style, ...props}) {
        const thumbStyle = {};
        return (
            <div
                className={classes.scrollingBody}
                style={{...style, ...thumbStyle}}
                {...props}></div>
        );
    }

    render() {
        return (
            <div className={classes.scrollingBody}>
                <Scrollbar
                    thumbSize={150}
                    autoHeight={false}
                    autoHide={false}
                    renderTrackVertical={this.renderTrack}
                    renderThumbVertical={this.renderThumb}
                >
                    {this.props.children}
                </Scrollbar>
            </div>
        );

    }
}

export default ScrollingBody;
