import { connect } from 'react-redux';

import JebloButton from './JebloButton';
import {sendRequestForInfo} from 'redux/hangar';

const mapStateToProps = (state) => ({
    fetching: state.hangar.fetching
});

export default connect(mapStateToProps,  {sendRequestForInfo})(JebloButton);