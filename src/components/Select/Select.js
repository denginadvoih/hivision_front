/**
 * Created by user on 17.05.2016.
 */
import React from 'react';
import cx from 'classnames';
import _ from 'lodash';
import PropTypes from "prop-types";
import Scrollbar from "react-scrollbars-custom";

class Select extends React.Component {
    state = {
        open: false,
        outerClose: false,
        placeholder: 'Выберите значение',
        randomId: 'select' + Math.floor(Math.random() * 9000 + 1000),
        addCustom: false,
        customValue: ''
    };

    componentDidUpdate(prevProps, prevState) {
        if (this.state.open && !prevState.open) {
            let el = document.getElementById('root');
            el.addEventListener('click', this.close);
        }
        if (!this.state.open && prevState.open) {
            let el = document.getElementById('root');
            el.removeEventListener('click', this.close);
        }
    };

    componentWillUnmount() {
        let el = document.getElementById('root');
        el.removeEventListener('click', this.close);
    };

    close() {
        this.setState({open: false});
    };

    open() {
        this.setState({open: true});
    };

    changeValue(selected) {
        if (selected.type === 'item') {
            this.props.onChange(selected);
            this.setState({open: false});
        }
    };

    changeCustomValue(e) {
        this.setState({customValue: e.target.value});
        let value = {type: 'item', label: e.target.value, value: e.target.value};
        this.props.onChange(value);
    };

    toggleOpen(e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.state)
            if (!this.props.disabled && !this.state.open) {
                this.setState({open: !this.state.open});
            }
    };

    renderTrack({style, ...props}) {
        const trackStyle = {};
        return (
            <div className="scrollTrack"
                 style={{...style, ...trackStyle}}
                 {...props}></div>
        );
    };

    renderThumb({style, ...props}) {
        const thumbStyle = {};
        return (
            <div
                className="scrollThumb"
                style={{...style, ...thumbStyle}}
                {...props}></div>
        );
    };

    switchCustom() {
        this.setState({addCustom: !this.state.addCustom});
    };

    render() {
        let selectedValue;
        if (_.isObjectLike(this.props.value)) {
            selectedValue = _.find(this.props.options, option => {
                return _.isEqual(option.value, this.props.value);
            });
        } else {
            selectedValue = _.find(this.props.options, option => {
                return option.value === this.props.value;
            });
        }
        if (!selectedValue) {
            selectedValue = {value: this.props.value, label: this.props.value, type: 'item'};
        }
        let addCustom;
        if (this.props.withCustom) {
            addCustom = <div className="selectComponentCustom" onClick={this.switchCustom}>
                {this.props.customText}
            </div>;
        }
        if (this.state.addCustom) {
            return <div className="customSelect selectReason">
                <div className="control">
                    <input
                        type="text"
                        name="customReason"
                        value={this.state.customValue}
                        onChange={this.changeCustomValue}
                        onKeyUp={e => {
                            if (e.key === 'Escape' || e.key === 'Enter') {
                                this.setState({addCustom: false});
                            }
                        }}
                    />
                    <div className="arrow" onClick={this.switchCustom}></div>
                </div>
            </div>;
        }
        return (<div
            className={cx({
                'customSelect': true,
                'disabled': this.props.disabled,
                [this.props.className]: this.props.className,
                [this.props.type]: true,
                'withIcon': this.props.withIcon,
                'opened': this.state.open

            })}>
            {this.state.open ? (
                    <div key="opened" className="control">
                        <div className="icon"/>
                        {
                            (this.props.value || this.props.value === 0) ? (
                                    <div className="value">{selectedValue.label}</div>)
                                : (<div className="placeHolder">{this.props.placeholder
                                ? this.props.placeholder
                                : this.state.placeholder}</div>)
                        }
                        <div className="arrow" id={this.state.randomId}></div>
                    </div>
                ) :
                (
                    <div key="closed" className="control" onClick={() => this.toggleOpen()}>
                        <div className="icon"/>
                        {
                            (this.props.value || this.props.value === 0) ? (
                                    <div className="value">{selectedValue.label}</div>)
                                : (<div className="placeHolder">{this.props.placeholder
                                ? this.props.placeholder
                                : this.state.placeholder}</div>)
                        }
                        <div className={cx({'arrow': true, 'icon': this.props.icon})} id={this.state.randomId}></div>
                    </div>
                )
            }
            {
                _.isEmpty(this.props.error)
                    ? null
                    : (<div className="error">
                        <div className="wrapper">
                            {this.props.error}
                        </div>
                    </div>)
            }

            <div
                className={cx({
                    'customSelect__outerMenu': true,
                    [this.props.className]: true,
                    [this.props.type]: true,
                    'closed': !this.state.open,
                    'withIcon': this.props.icon
                })}>
                <Scrollbar
                    autoHeight={true}
                    autoHide={false}
                    renderTrackVertical={this.renderTrack}
                    renderThumbVertical={this.renderThumb}
                >
                    {(this.props.options || []).map(option => {
                        return (
                            <div
                                key={JSON.stringify(option.value)}
                                className={option.type}
                                onClick={this.changeValue.bind(this, option)}>
                                {option.label}
                            </div>);
                    })}
                </Scrollbar>
                {addCustom}
            </div>
        </div>);
    };
}

Select.propTypes = {
    name: PropTypes.string,
    value: PropTypes.any,
    options: PropTypes.array.isRequired,
    type: PropTypes.string.isRequired,//byLink - ссылка, bySelect - список
    onChange: PropTypes.func.isRequired
};

export default Select;
