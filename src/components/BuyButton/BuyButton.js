import React, {useEffect, useState} from 'react';
import ym from 'react-yandex-metrika';
import classes from './styles.module.scss';
import {Link} from "react-router-dom";
import {BUY_LINKS} from "utils/constants";

const BuyButton = () => {
    const [open, setOpen] = useState();
    useEffect(() => {
        const outsideClick = () => {
        setOpen(false);
        }
        document.addEventListener('click', outsideClick);
        return () => {
            document.removeEventListener('click', outsideClick);
        }
    }, []);
    return <div className={classes.buyButton} onClick={(e) => e.stopPropagation() }>
        <div className={classes.linksWrapper + ' ' + (open ? classes.open : '')}>
            <div className={classes.links}>
                <Link to={BUY_LINKS.wildberries} target={'_blank'} className={classes.wildberries} onClick={() => {ym('reachGoal', 'click_wildberries');}}/>
                <Link to={BUY_LINKS.ozon} target={'_blank'} className={classes.ozon} onClick={() => ym('reachGoal','ozon_click')}/>
                <Link to={BUY_LINKS.farpost} target={'_blank'} className={classes.farpost} onClick={() => ym('reachGoal','farpost_click')}/>
                <Link to={BUY_LINKS.whatsapp} target={'_blank'} className={classes.whatsapp} onClick={() => ym('reachGoal','click_whatsapp')}/>
            </div>
        </div>
        <div className={classes.mainWrapper} onClick={() => setOpen((_) => !_)}>
            <div className={classes.main}>Купить</div>
            <div className={classes.circleOuter}/>
            <div className={classes.circleFill}/>
        </div>
    </div>
}

export default BuyButton;
