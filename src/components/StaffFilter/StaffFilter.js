import React from 'react'

import Select from 'components/Select';

import './stafffilter.module.scss'

class StaffFilter extends React.Component {

    state = {
        fieldValues: {}
    }

    render() {
        return (
            <div className="staffFilter">
                {this.props.fields.map(field => {
                    if (field.type === 'reference') {
                        let items = Object.values(field.reference.items || {}).filter(i => i.exist === true).map(i => {
                            return {type: 'item', value: i.id, label: i.title};
                        });
                        return (
                            <div className={"staffFilter__field staffFilter__field--" + field.label}>
                                <Select
                                    withIcon={true}
                                    options={items}
                                    type="select"
                                    placeholder={field.title}
                                    value={this.state.fieldValues[field.id] || null}
                                    onChange={(selected) => {
                                        this.setState({
                                            fieldValues: {
                                                ...this.state.fieldValues,
                                                [field.id]: selected.value
                                            }
                                        });
                                        if (this.props.onChange) {
                                            this.props.onChange(field.id, selected.value);
                                        }
                                    }}/>
                            </div>
                        );
                    } else {
                        return null;
                    }
                })}
            </div>
        );

    }
}

export default StaffFilter;
