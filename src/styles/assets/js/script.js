$(document).ready(function () {
    $('.hamburger').click(function() {
        $(this).toggleClass('closed');
        $(this).siblings('.links').toggleClass('closed');
    });
    $('#search_button').click(function() {
        $(this).toggleClass('closed');
        $('#search_fields').toggleClass('closed');
    })
});